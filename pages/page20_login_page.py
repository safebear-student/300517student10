from page_objects import PageObject, PageElement


class LoginPage(PageObject):
    username_field = PageElement(id_="myid")
    password_field = PageElement(id_="mypass")

    def check_page(self):
        return "Sign In" in self.w.title


    def login(self, mainpage, username, password):
        self.username_field.send_keys(username)
        self.password_field.send_keys(password)
        self.password_field.submit()
        return mainpage.check_page()
